For all your building materials, timber & DIY needs. 
When you trade with Buildbase, you�ll get everything you need in one place. We take pride in serving the local tradespeople and our communities, working with our customers to get the job done, so you can rely on us to give a brilliant service.
|| Address: Atlas Mill, Bentinck Street, Chorley Old Road, Bolton BL1 4QG, United Kingdom || Phone: +44 1204 493961 || Website: https://www.buildbase.co.uk/storefinder/store/Bolton-1191
